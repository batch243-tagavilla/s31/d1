/* 
    CREATE A ROUTE
        "/greeting"
*/

const http = require("http");
const port = 4000;
const url = require("url")

const server = http.createServer( (request, response) => {
    if(request.url == "/greeting") {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`Hello again!`);
    } else if (request.url == "/homepage") {
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`Welcome Home`);
    } else {
        response.writeHead(400, {"Content-Type":"text/plain"});
        response.end(`Page not Available`);
    }
});

server.listen(port);

// When the server is running, console will print this message"
console.log(`Server running at localhost:${port}`);