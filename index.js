// Use the "require" directive to load the HTTP module of node JS
// A "modules" is a software component or part of a program that contains one or more routines
// The "http module" lets Node.js transfer data using the HTTP (Hypertext Transfer Protocol)
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (browser) and servers (nodeJS/ExpressJS applications) communicate exchanging individual messages

// REQUEST - messages sent by the client (usually in a web browser)
// RESPONSES - messages sent by the server as an answer to the client

    let http = require("http")

    // Create a server
    /* 
        - the http module has a createServer() method that accepts a function as an argument and allows server creation
        - the arguments passed in the function are request and request objects that contains methods that allows us to receive requrest from the client and send the responses back
        - using the module's createServer() method, we can create an HTTP server that listens to the requests on a specified port and gives back to the client
    */

    // Define the port number that the server will be listening to
    http.createServer(function(request, response){
        response.writeHead(200, {"Content-Type":"text/plain"});
        response.end(`Hello Kitty`);
    }).listen(4000)

    console.log(`Server running at localhost:4000`);

    // A port is virtual point where network connections start and end

    // Send a response back to client
        /* 
            - use the writeHead() method
            - set a status code for the response
        */

    /* Inputting the command
        node index.js 
    */

    /* Install nodemon via terminal
        npm install -g nodemon (windows)
        sudo npm install -g nodemon (linux) 
    */

    /* To check if nodemon is already isntalled via terminal
        nodemon -v 
    */

    /* IMPORTANT NOTE */
    /*
        - installing the package will allows the server to automatically restart when files have been changed or updated
        - "-g" refers to a global installation  where the current version of the package/dependency will be installed locally on our device allowing us to use nodemon on any project
    */